const multer = require('multer');
var fn = '';
// const UserModel = require('../module/user/models/user.models');
const pool = require('../config/lib/db');

const dateString = Date.now(); 

var storage = multer.diskStorage( {
    destination: function (req, file, cb) {
        query1 = "call file_upload(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
            } else {
                conn.query(query1, [
                    req.params.Id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                       
                    } else {
                        //console.log(row[0]);
                        fn = row[0][0].Fname;
                        conn.release();
                        cb(null, '/home/anand/Downloads/joli-admin-master/EMR/drive/'+fn);
                    }
                });
            }
        },
        );
    },
    filename: function (req, file, cb) {
        cb(null,  dateString + file.originalname);
        query1 = "call file_storage	(?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
            } else {
                conn.query(query1, [
                    req.params.Id,
                    dateString + file.originalname
                ], (err, row) => {
                    if (err) {
                        conn.release();
                       
                    } else {
                        console.log(row[0]);
                        conn.release();
                    }
                });
            }
        },
        );
    }
},
);

const upload = multer({
    storage: storage,
    // fileFilter: fileFilter,
    // limits: {
    //     fileSize: 1024 * 1024 * 5
    // }
});

module.exports = {
    upload
};
