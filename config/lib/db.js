const mysql = require('mysql');
const constants = require('../../common/constant');
require('dotenv').config()


var pool = mysql.createPool({
  connectionLimit: process.env.MYSQL_ALLOWED_CONNECTIONS,
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  insecureAuth : true
});

if (pool) {
  console.log("connection created......");
} else {
  console.log("problem in connection");
}

setInterval(keepalive, constants.KEEPALIVE_TIME);


pool.on('connection', function (connection) {
  connection.query('SET SESSION auto_increment_increment=1');
});

pool.on('acquire', function (connection) {
  console.log('Connection %d acquired', connection.threadId);
});

pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});

pool.on('release', function (connection) {
  console.log('Connection %d released', connection.threadId);
});

function keepalive() {
  pool._freeConnections.forEach((connection) =>
    connection.query('SELECT 1 ', function (err) {
      if (err) {
        logger.info(err.code);
      }
      logger.info('Keepalive RDS connection pool using connection id', connection.threadId);
    })
  );
}


module.exports = pool;