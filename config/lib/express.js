const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
require('dotenv').config();
const pool = require('./db');
const Login = require('../../module/login/route/Login.route');
const Client = require('../../module/client/route/client.route');
const user = require('../../module/user/route/user.route');
const path = require('path');


//init app
const app = express();

// app.use("/drive", express.static(__dirname + '/drive'));

const filename = '../../drive/';
app.use(express.static(path.join(__dirname, filename)));

//port number
const port = process.env.PORT;
console.log('Your port is :' ,port);

//set port to express
app.set("port", port);

//bodyparser configuration
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(cors());
app.use(bodyParser.json());

//log information
app.use(morgan("dev"));

// app.use(express.static(path.join(__dirname, 'upload')));

app.use('/api', Login);
app.use('/api',Client);
app.use('/api',user);

//error handeling for api
app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

//cors configuration
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header(
            "Access-Control-Allow-Headers",
            "PUT, POST, PATCH, GET",
            "DELETE"
        );
        return res.status(200).json({});
    }
});
//assigning port to server.
app.listen(port, () => {
    console.log("your server is start on port: ", port);
});



module.exports = app;