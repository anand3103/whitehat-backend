const LoginModel = require('../models/Login.models');


exports.Login = async (req, res) => {
    try {
        const data = await LoginModel.login(req.body);
        console.log('data', data);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your data not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                data: data
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
}


exports.findAllClinics = async (req, res) => {
    try {
        const data = await ClinicModel.findAllClinics();
        console.log('data', data);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your data not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                data: data
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
};
exports.findClinicById = async (req, res) => {
    try {
        const data = await ClinicModel.findClinicById(req.params.id);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your id is not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                data: data
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
};
exports.findClinicByName = async (req, res) => {
    try {
        const data = await ClinicModel.findClinicByName(req.params.clinicName);
        console.log(data);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your name is not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                data: data
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
};
exports.insertClinic = async (req, res) => {
    try {
        const data = await ClinicModel.insertClinic(req.body);
        console.log(data);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your data is not inserted into database successfully."
            });
        } else {
            res.status(200).json({
                status: 200,
                message: "Your data is inserted into database successfully.",
                data: data[0]
            });
        }
    } catch (error) {
        res.status(404).json(err);
    }
};
exports.updateClinic = async (req, res) => {
    try {
        const data = await ClinicModel.updateClinic(req.body);
        // console.log(data);
        if (data[1].affectedRows == 0) {
            res.status(404).json({
                status: 404,
                message: "Your data is not updated into database successfully."
            });
        } else {
            res.status(200).json({
                status: 200,
                message: "Your data is updated into database successfully.",
                data: data[0]
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
};
exports.delelteClinicById = async (req, res) => {
    try {
        const data = await ClinicModel.delelteClinicById(req.params.id)
        console.log(data);
        if (data.affectedRows == 0) {
            res.status(404).json({
                status: 404,
                message: "Your " + req.params.id + " is not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                message: "Your id: " + req.params.id + " is  deleted successfully."
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
};