const express = require('express');
const LoginController = require('../controller/Login.controller');
const validator = require('../validator/clinic.validator');

const router = express.Router();

router.post('/login', LoginController.Login);

module.exports = router;