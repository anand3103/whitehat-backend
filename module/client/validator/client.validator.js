const Joi = require('@hapi/joi');


exports.validateInsertClinic = async (req, res, next) => {
    const schema = Joi.object().keys({
        clinicName: Joi.string().required(),
        clinicAddress: Joi.string().required(),
        clinicEmailId: Joi.string().email().required(),
        clinicContactNumber: Joi.number().integer().required(),
        numberOfDoctors: Joi.number().integer().required(),
        clinicRegistrationNumber: Joi.string().required(),
    });
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json("Error: " + err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};

exports.validateUpdateClinic = async (req, res, next) => {
    const schema = Joi.object().keys({
        id: Joi.number().integer().required(),
        clinicName: Joi.string().required(),
        clinicAddress: Joi.string().required(),
        clinicEmailId: Joi.string().email().required(),
        clinicContactNumber: Joi.number().integer().required(),
        numberOfDoctors: Joi.number().integer().required(),
        clinicRegistrationNumber: Joi.string().required(),
    });
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json("Error: " + err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};