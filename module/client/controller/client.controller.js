const ClientModel = require('../models/client.models');


exports.getAllClients = async (req, res) => {
    try {
        const data = await ClientModel.getAllClients(req.params.id);
        console.log('data2222', data);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your data not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                data: data
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
};
exports.addClient = async (req, res) => {
    try {
        const data = await ClientModel.addClient(req.body);

        res.status(200).json({
            status: 200,
            message: "Your data is inserted into database successfully.",
            data: data[0]
        });

    } catch (error) {
        res.status(404).json(error);
    }
};

exports.updateClient = async(req,res) =>{
    try {
        const data = await ClientModel.updateClient(req.params.check,req.params.id);
        res.send(data);
        
    } catch (error) {
        res.status(404).json(error);
    }
}