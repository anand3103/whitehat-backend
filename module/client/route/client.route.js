const express = require('express');
const clientController = require('../controller/client.controller');
const validator = require('../validator/client.validator');

const router = express.Router();

router.get("/getAllClients/:id", clientController.getAllClients);

router.post("/addClient", clientController.addClient);

router.put('/updateClient/:check/:id',clientController.updateClient);

module.exports = router;