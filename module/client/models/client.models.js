const pool = require('../../../config/lib/db');
const bcrypt = require('bcrypt');
const saltRounds = 6;
var Promise = require('promise');
var Userid;
var fs = require('fs');

exports.getAllClients = (id) => {
    return new Promise((resolve, reject) => {
        query1 = "select * from login where Usertype = ?";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [id], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        
                        console.log("row", row);
                        resolve(JSON.parse(JSON.stringify(row)));
                        conn.release(); 
                    }
                });
            }
        });
    })
}

exports.addClient = (body) => {
    console.log(body);
    var salt = bcrypt.genSaltSync(saltRounds);
    var hash = bcrypt.hashSync(body.email, salt);
    console.log('encrypt:', hash);
    return new Promise((resolve, reject) => {
        query1 = "insert into login (Fname,Lname,Email,Mobile,Password,Salt,Status,Usertype) values (?,?,?,?,?,?,?,?)";
        query2 = "select Id from login where Id = (SELECT LAST_INSERT_ID())";
        query3 = "insert into access (Userid, Read_access, Write_access) values (?,?,?)";       
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    body.firstName,
                    body.lastName,
                    body.email,
                    body.mobile,
                    body.password,
                    hash, '1', '2'
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        var dir = './drive/'+body.firstName;
                        fs.mkdirSync(dir);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });
}

exports.updateClient = (check, id) => {

    console.log('check:', check, 'id:', id);

    return new Promise((resolve, reject) => {
        query1 = "update login set Status = ? where Id = ?";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    check,
                    id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        // console.log(row);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });

}

