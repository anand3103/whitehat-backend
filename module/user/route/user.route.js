const express = require('express');
const UserController = require('../controller/user.controller');
const validator = require('../validator/user.validator');
const multer = require('../../../helper/multer.helper');

const router = express.Router();

router.post('/login', UserController.Login);

router.post('/addUser', UserController.addUser);

router.put('/updateUser/:check/:id',UserController.updateUser);

router.post('/createDirectory/:company/:dirname/:id',UserController.createDirectory);

router.post('/fileUpload/:Id',multer.upload.single('file'));

router.get('/getCompanyName/:Id',UserController.getCompanyName);

router.post('/userAccess/:id', UserController.userAccess);

router.post('/getCompanyImages/:id', UserController.getCompanyImages);

router.post('/getCompanyFolders/:id', UserController.getCompanyFolders);

module.exports = router;