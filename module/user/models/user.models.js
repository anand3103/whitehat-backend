const pool = require('../../../config/lib/db');
var Promise = require('promise');
const bcrypt = require('bcrypt')
const saltRounds = 15

exports.login = (body) => {
    console.log('sql query', body)
    return new Promise((resolve, reject) => {
        query1 = "select * from login where Email = ? &&  Password = ?";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [body.Email, body.Password], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        conn.release();
                        console.log("row", row);
                        resolve(JSON.parse(JSON.stringify(row[0])));
                    }
                });
            }
        });
    })
}



exports.addUser = (body) => {
    console.log(body);
    var salt = bcrypt.genSaltSync(saltRounds);
    var hash = bcrypt.hashSync(body.email, salt);
    console.log('encrypt:', hash);
    return new Promise((resolve, reject) => {
        const query1 = "call add_user(?,?,?,?,?,?,?,?,?,?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    body.firstName,
                    body.lastName,
                    body.email,
                    body.mobile,
                    body.password,
                    hash, '1', '3',
                    body.companyname,
                    body.read,
                    body.write
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject();
                    } else {
                        // console.log(row);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });
};

exports.updateUser = (check, id) => {
    return new Promise((resolve, reject) => {
        query1 = "update login set Status = ? where Id = ?";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    check,
                    id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        // console.log(row);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });

}

exports.fileUpload = (id) => {
    return new Promise((resolve, reject) => {
        query1 = "call file_upload(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        //console.log(row[0]);
                        resolve(row[0]);
                        conn.release();
                    }
                });
            }
        },
        );
    });
}

exports.getCompanyName = (Id) => {
    return new Promise((resolve, reject) => {
        query1 = "call file_upload(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    Id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        //console.log(row[0]);
                        resolve(row[0]);
                        conn.release();
                    }
                });
            }
        },
        );
    });

}

exports.createDirectory=(dirname,id)=>{
    return new Promise((resolve, reject) => {
        query1 = "call user_folder(?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    id,dirname
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        //console.log(row[0]);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });
}

exports.userAccess=(id)=>{
    return new Promise((resolve, reject) => {
        query1 = "call user_access(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    id,
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        //console.log(row[0]);
                        resolve(row[0]);
                        conn.release();
                    }
                });
            }
        },
        );
    });
};

exports.getCompanyImages=(id)=>{
    return new Promise((resolve, reject) => {
        query1 = "call get_user_images(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    id,
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        //console.log(row[0]);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });
};

exports.getCompanyFolders=(id)=>{
    return new Promise((resolve, reject) => {
        query1 = "call get_user_folders(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, [
                    id,
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        //console.log(row[0]);
                        resolve(row);
                        conn.release();
                    }
                });
            }
        },
        );
    });
}