const UserModel = require('../models/user.models');
const fs = require('fs');
const fname = require('../../../helper/multer.helper');
var path = require('path');




exports.Login = async (req, res) => {
    try {
        const data = await UserModel.login(req.body);
        console.log('data', data);
        if (data.length == 0) {
            res.status(404).json({
                status: 404,
                message: "Your data not find into database."
            });
        } else {
            res.status(200).json({
                status: 200,
                data: data
            });
        }
    } catch (error) {
        res.status(404).json(error);
    }
}


exports.addUser = async (req, res) => {
    try {
        const data = await UserModel.addUser(req.body);
        console.log(data);

        res.status(200).json({
            status: 200,
            message: "Your data is inserted into database successfully.",
            data: data[0]
        });
    } catch (error) {
        res.status(404).json(error);
    }
};

exports.updateUser = async (req, res) => {
    try {
        const data = await UserModel.updateUser(req.params.check, req.params.id);

        console.log('data', data);

    } catch (error) {
        res.status(404).json(err);
    }   
}


exports.fileUpload = async (req, res) => {
    try {
        const data = await UserModel.fileUpload(req.params.id);
        console.log('data', data);
        console.log('file is here', req.file);
        // if(data.length > 0){
        //     console.log('file is here', req.file);
        // }
        res.status(200).json({
            status:200,
            data:data
        });

    } catch (error) {
        res.status(404).json(error);
    }
}

exports.createDirectory = async (req, res) => {
    console.log(req.params.dirname);
    var dir = 'drive/'+ req.params.company+'/' + req.params.dirname
    var parentDir = path.dirname(dir);
    console.log(parentDir);
    
    fs.mkdirSync(dir);
    data = await UserModel.createDirectory(req.params.dirname,req.params.id)
    // res.send("succesfully")
}

exports.getCompanyName = async(req,res)=>{
    try {
        console.log(req.params.Id);
        data = await UserModel.getCompanyName(req.params.Id);
        console.log(data[0].Fname);
        res.status(200).json({
            status:200,
            data:data[0].Fname
        });
    } catch (error) {
        res.status(404).send(error);
    }
}

exports.userAccess= async(req,res)=>{
    try {
        console.log(req.params.id);
        data = await UserModel.userAccess(req.params.id);
        console.log(data[0]);
        res.status(200).json({
            status:200,
            data:data[0]
        });
    } catch (error) {
        res.status(404).send(error);
    }
}

exports.getCompanyImages = async(req,res)=>{
    try {
        console.log(req.params.id);
        data = await UserModel.getCompanyImages(req.params.id);
        console.log(data);
        res.status(200).json({
            status:200,
            data:[data[0],data[1]]
        });
    } catch (error) {
        res.status(404).send(error);
    }
}

exports.getCompanyFolders = async(req, res)=>{
    try {
        console.log(req.params.id);
        data = await UserModel.getCompanyFolders(req.params.id);
        console.log(data);
        res.status(200).json({
            status:200,
            data:[data[0],data[1]]
        });
    } catch (error) {
        res.status(404).send(error);
    }
}